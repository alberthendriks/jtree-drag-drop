# JTree-Drag-Drop

## introduction

This project exists only to show that the author can find his way in technologies such as Swing's JTree with little
prior experience. The last time worked with Swing was in 2008 during University education.

It took two days from scratch to find and test the original source code by Craig Wood at 
https://coderanch.com/t/346509/java/JTree-drag-drop-tree-Java and improve that source code.
The code is also mentioned on https://stackoverflow.com/questions/4588109/drag-and-drop-nodes-in-jtree. The current
author's contributions are mentioned in the next section and can be verified by checking the commit history of this
repo.

As an interesting sidenote, significantly more lines were removed than added.

## Solved bugs and improved functionality

- Dragged subtrees of depth two or higher got flattened.
In the below screenshot, the subtree of violet was dragged
inside yellow:
![screenshot](img/screenshot-bug1.png)
This bug is now solved.

- When dragging a subtree, all subitems needed to be selected. As can be seen in the above screenshot, violet, blue and
red needed to be selected all to drag the subtree starting at violet. In the current version, only violet needs to be
dragged to move its entire subtree.

## Remaining poor functionality

When dragging an expanded subtree, that subtree gets collapsed. This remains unsolved as of yet. The subtree on the right hand side of the above screenshot was manually expanded.

## Potential future work

This demo code was constructed in a short period and does of course not show all intricacies of creating a Swing
application. Things that come to mind are:

- Clean separation of model and Gui interface elements
- Tweaking the Gui
- Using ActionListeners
- Clean error handling
- Making calls to a backend
- Creating new Gui elements
- Painting on a canvas
- User authentication with a backend
- Adding automated testing